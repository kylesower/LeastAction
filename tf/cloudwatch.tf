import {
  to = aws_cloudwatch_log_group.web
  id = "cloudfront-leastaction"
}

resource "aws_cloudwatch_log_group" "web" {
  provider = aws.e1
  name = "cloudfront-leastaction"
  retention_in_days = 180
}
