terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws",
      version = "~> 5.84",
    }
  }
}

provider "aws" {
  alias  = "e1"
  region = "us-east-1"
}

provider "aws" {
  region = "us-east-2"
}
