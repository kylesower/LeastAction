resource "aws_acm_certificate" "web" {
  provider = aws.e1
  domain_name = "leastaction.me"
  subject_alternative_names = [
    "www.leastaction.me",
    "leastaction.me",
  ]
  validation_method = "DNS"
  key_algorithm = "RSA_2048"
}

