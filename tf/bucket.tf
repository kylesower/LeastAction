resource "aws_s3_bucket" "web" {
  bucket = "leastaction.me"
}

resource "aws_s3_bucket_public_access_block" "web" {
  bucket                  = aws_s3_bucket.web.id
  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

resource "aws_s3_bucket_policy" "web" {
  bucket = aws_s3_bucket.web.id
  policy = data.aws_iam_policy_document.web.json
}

data "aws_iam_policy_document" "web" {
  statement {
    principals {
      type        = "Service"
      identifiers = ["cloudfront.amazonaws.com"]
    }

    actions = [
      "s3:GetObject"
    ]
    effect = "Allow"

    resources = [
      "${aws_s3_bucket.web.arn}/*"
    ]
    condition {
      values   = [aws_cloudfront_distribution.web.arn]
      test     = "StringEquals"
      variable = "AWS:SourceArn"
    }
  }
}
