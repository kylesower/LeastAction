import os
import shutil
import re
import random


def add_ver_to_html(new_html_path, ver, files):
    with open(new_html_path, 'r+') as f:
        lines = f.read()
    with open(new_html_path, 'w+') as f:
        for file in files:
            if file.endswith(".js") or file.endswith(".css"):
                lines = lines.replace(file, ver + file)
        f.write(lines)


curr_dir = os.getcwd()
www_dir = os.path.join(curr_dir, "www")
web_ignore = os.path.join(curr_dir, ".webignore")
with open(web_ignore, 'r') as f:
    ignore_files = list(filter(lambda x: len(x) != 0, f.read().split('\n')))

build_path = os.path.join(curr_dir, "build")
try: 
    os.mkdir(build_path)
except FileExistsError as e:
    shutil.rmtree(build_path)
    os.mkdir(build_path)

shutil.copytree(www_dir, build_path, dirs_exist_ok=True)
js_and_css_files = []
for dirpath, dirnames, filenames in os.walk(build_path):
    for filename in filenames:
        if filename.endswith(".js") or filename.endswith(".css"):
            js_and_css_files.append(filename)

ver = "v" + "".join([hex(b)[2:] for b in random.randbytes(4)]) + "_"
for dirpath, dirnames, filenames in os.walk(build_path):
    for filename in filenames:
        if filename in ignore_files:
            continue
        filepath = os.path.join(dirpath, filename)
        if filename.endswith(".html"):
            add_ver_to_html(filepath, ver, js_and_css_files)
        if filename.endswith(".js") or filename.endswith(".css"):
            shutil.move(filepath, os.path.join(dirpath, ver + filename))
