build: $(shell find www -type f) $(shell find wasm -type f)
	rm -rf www/pkg
	cd wasm \
		&& cargo +nightly build -Zbuild-std=std,panic_abort -r --lib --target wasm32-unknown-unknown \
		&& wasm-bindgen ./target/wasm32-unknown-unknown/release/*.wasm --out-dir ../www/pkg --target web
	python version_files.py

deploy: build
	cd tf && terraform apply
	# TODO: delete old files
	aws s3 cp --recursive build s3://leastaction.me
