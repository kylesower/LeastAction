let wasm;

const cachedTextDecoder = (typeof TextDecoder !== 'undefined' ? new TextDecoder('utf-8', { ignoreBOM: true, fatal: true }) : { decode: () => { throw Error('TextDecoder not available') } } );

if (typeof TextDecoder !== 'undefined') { cachedTextDecoder.decode(); };

let cachedUint8ArrayMemory0 = null;

function getUint8ArrayMemory0() {
    if (cachedUint8ArrayMemory0 === null || cachedUint8ArrayMemory0.byteLength === 0) {
        cachedUint8ArrayMemory0 = new Uint8Array(wasm.memory.buffer);
    }
    return cachedUint8ArrayMemory0;
}

function getStringFromWasm0(ptr, len) {
    ptr = ptr >>> 0;
    return cachedTextDecoder.decode(getUint8ArrayMemory0().subarray(ptr, ptr + len));
}

function _assertClass(instance, klass) {
    if (!(instance instanceof klass)) {
        throw new Error(`expected instance of ${klass.name}`);
    }
}
/**
 * @param {Pend} p1
 * @param {Pend} p2
 * @returns {number}
 */
export function calc_E(p1, p2) {
    _assertClass(p1, Pend);
    _assertClass(p2, Pend);
    const ret = wasm.calc_E(p1.__wbg_ptr, p2.__wbg_ptr);
    return ret;
}

/**
 * @param {Pend} p1
 * @param {Pend} p2
 * @param {number} width
 * @returns {number}
 */
export function calc_w_max(p1, p2, width) {
    _assertClass(p1, Pend);
    _assertClass(p2, Pend);
    const ret = wasm.calc_w_max(p1.__wbg_ptr, p2.__wbg_ptr, width);
    return ret;
}

/**
 * @param {Pend} p1
 * @param {Pend} p2
 * @param {number} dt
 * @returns {AngleVals}
 */
export function rk4(p1, p2, dt) {
    _assertClass(p1, Pend);
    _assertClass(p2, Pend);
    const ret = wasm.rk4(p1.__wbg_ptr, p2.__wbg_ptr, dt);
    return AngleVals.__wrap(ret);
}

/**
 * @param {Pend} p1
 * @param {Pend} p2
 * @param {number} w
 * @param {number} wmax
 * @param {number} wscale
 * @returns {CanvasVals}
 */
export function normalize_to_canvas(p1, p2, w, wmax, wscale) {
    _assertClass(p1, Pend);
    _assertClass(p2, Pend);
    const ret = wasm.normalize_to_canvas(p1.__wbg_ptr, p2.__wbg_ptr, w, wmax, wscale);
    return CanvasVals.__wrap(ret);
}

/**
 * @param {AttractorVals} a
 * @param {number} width
 * @param {number} frame_count
 * @returns {AttractorVals}
 */
export function rotate_and_center(a, width, frame_count) {
    _assertClass(a, AttractorVals);
    const ret = wasm.rotate_and_center(a.__wbg_ptr, width, frame_count);
    return AttractorVals.__wrap(ret);
}

const AngleValsFinalization = (typeof FinalizationRegistry === 'undefined')
    ? { register: () => {}, unregister: () => {} }
    : new FinalizationRegistry(ptr => wasm.__wbg_anglevals_free(ptr >>> 0, 1));

export class AngleVals {

    static __wrap(ptr) {
        ptr = ptr >>> 0;
        const obj = Object.create(AngleVals.prototype);
        obj.__wbg_ptr = ptr;
        AngleValsFinalization.register(obj, obj.__wbg_ptr, obj);
        return obj;
    }

    __destroy_into_raw() {
        const ptr = this.__wbg_ptr;
        this.__wbg_ptr = 0;
        AngleValsFinalization.unregister(this);
        return ptr;
    }

    free() {
        const ptr = this.__destroy_into_raw();
        wasm.__wbg_anglevals_free(ptr, 0);
    }
    /**
     * @returns {number}
     */
    get t1() {
        const ret = wasm.__wbg_get_anglevals_t1(this.__wbg_ptr);
        return ret;
    }
    /**
     * @param {number} arg0
     */
    set t1(arg0) {
        wasm.__wbg_set_anglevals_t1(this.__wbg_ptr, arg0);
    }
    /**
     * @returns {number}
     */
    get w1() {
        const ret = wasm.__wbg_get_anglevals_w1(this.__wbg_ptr);
        return ret;
    }
    /**
     * @param {number} arg0
     */
    set w1(arg0) {
        wasm.__wbg_set_anglevals_w1(this.__wbg_ptr, arg0);
    }
    /**
     * @returns {number}
     */
    get t2() {
        const ret = wasm.__wbg_get_anglevals_t2(this.__wbg_ptr);
        return ret;
    }
    /**
     * @param {number} arg0
     */
    set t2(arg0) {
        wasm.__wbg_set_anglevals_t2(this.__wbg_ptr, arg0);
    }
    /**
     * @returns {number}
     */
    get w2() {
        const ret = wasm.__wbg_get_anglevals_w2(this.__wbg_ptr);
        return ret;
    }
    /**
     * @param {number} arg0
     */
    set w2(arg0) {
        wasm.__wbg_set_anglevals_w2(this.__wbg_ptr, arg0);
    }
}

const AttractorFinalization = (typeof FinalizationRegistry === 'undefined')
    ? { register: () => {}, unregister: () => {} }
    : new FinalizationRegistry(ptr => wasm.__wbg_attractor_free(ptr >>> 0, 1));

export class Attractor {

    static __wrap(ptr) {
        ptr = ptr >>> 0;
        const obj = Object.create(Attractor.prototype);
        obj.__wbg_ptr = ptr;
        AttractorFinalization.register(obj, obj.__wbg_ptr, obj);
        return obj;
    }

    __destroy_into_raw() {
        const ptr = this.__wbg_ptr;
        this.__wbg_ptr = 0;
        AttractorFinalization.unregister(this);
        return ptr;
    }

    free() {
        const ptr = this.__destroy_into_raw();
        wasm.__wbg_attractor_free(ptr, 0);
    }
    /**
     * @returns {number}
     */
    get x() {
        const ret = wasm.__wbg_get_anglevals_t1(this.__wbg_ptr);
        return ret;
    }
    /**
     * @param {number} arg0
     */
    set x(arg0) {
        wasm.__wbg_set_anglevals_t1(this.__wbg_ptr, arg0);
    }
    /**
     * @returns {number}
     */
    get y() {
        const ret = wasm.__wbg_get_anglevals_w1(this.__wbg_ptr);
        return ret;
    }
    /**
     * @param {number} arg0
     */
    set y(arg0) {
        wasm.__wbg_set_anglevals_w1(this.__wbg_ptr, arg0);
    }
    /**
     * @returns {number}
     */
    get z() {
        const ret = wasm.__wbg_get_anglevals_t2(this.__wbg_ptr);
        return ret;
    }
    /**
     * @param {number} arg0
     */
    set z(arg0) {
        wasm.__wbg_set_anglevals_t2(this.__wbg_ptr, arg0);
    }
    /**
     * @returns {number}
     */
    get vx() {
        const ret = wasm.__wbg_get_anglevals_w2(this.__wbg_ptr);
        return ret;
    }
    /**
     * @param {number} arg0
     */
    set vx(arg0) {
        wasm.__wbg_set_anglevals_w2(this.__wbg_ptr, arg0);
    }
    /**
     * @returns {number}
     */
    get vy() {
        const ret = wasm.__wbg_get_attractor_vy(this.__wbg_ptr);
        return ret;
    }
    /**
     * @param {number} arg0
     */
    set vy(arg0) {
        wasm.__wbg_set_attractor_vy(this.__wbg_ptr, arg0);
    }
    /**
     * @returns {number}
     */
    get vz() {
        const ret = wasm.__wbg_get_attractor_vz(this.__wbg_ptr);
        return ret;
    }
    /**
     * @param {number} arg0
     */
    set vz(arg0) {
        wasm.__wbg_set_attractor_vz(this.__wbg_ptr, arg0);
    }
    /**
     * @returns {number}
     */
    get sigma() {
        const ret = wasm.__wbg_get_attractor_sigma(this.__wbg_ptr);
        return ret;
    }
    /**
     * @param {number} arg0
     */
    set sigma(arg0) {
        wasm.__wbg_set_attractor_sigma(this.__wbg_ptr, arg0);
    }
    /**
     * @returns {number}
     */
    get rho() {
        const ret = wasm.__wbg_get_attractor_rho(this.__wbg_ptr);
        return ret;
    }
    /**
     * @param {number} arg0
     */
    set rho(arg0) {
        wasm.__wbg_set_attractor_rho(this.__wbg_ptr, arg0);
    }
    /**
     * @returns {number}
     */
    get beta() {
        const ret = wasm.__wbg_get_attractor_beta(this.__wbg_ptr);
        return ret;
    }
    /**
     * @param {number} arg0
     */
    set beta(arg0) {
        wasm.__wbg_set_attractor_beta(this.__wbg_ptr, arg0);
    }
    /**
     * @param {number} x
     * @param {number} y
     * @param {number} z
     * @param {number} sigma
     * @param {number} rho
     * @param {number} beta
     * @returns {Attractor}
     */
    static new(x, y, z, sigma, rho, beta) {
        const ret = wasm.attractor_new(x, y, z, sigma, rho, beta);
        return Attractor.__wrap(ret);
    }
    update() {
        wasm.attractor_update(this.__wbg_ptr);
    }
    /**
     * @param {number} width
     * @returns {Point}
     */
    center(width) {
        const ret = wasm.attractor_center(this.__wbg_ptr, width);
        return Point.__wrap(ret);
    }
    /**
     * @param {number} width
     * @param {Point} center
     * @returns {AttractorVals}
     */
    scale_to_canvas(width, center) {
        _assertClass(center, Point);
        const ret = wasm.attractor_scale_to_canvas(this.__wbg_ptr, width, center.__wbg_ptr);
        return AttractorVals.__wrap(ret);
    }
    /**
     * @returns {string}
     */
    render() {
        let deferred1_0;
        let deferred1_1;
        try {
            const ret = wasm.attractor_render(this.__wbg_ptr);
            deferred1_0 = ret[0];
            deferred1_1 = ret[1];
            return getStringFromWasm0(ret[0], ret[1]);
        } finally {
            wasm.__wbindgen_free(deferred1_0, deferred1_1, 1);
        }
    }
}

const AttractorValsFinalization = (typeof FinalizationRegistry === 'undefined')
    ? { register: () => {}, unregister: () => {} }
    : new FinalizationRegistry(ptr => wasm.__wbg_attractorvals_free(ptr >>> 0, 1));

export class AttractorVals {

    static __wrap(ptr) {
        ptr = ptr >>> 0;
        const obj = Object.create(AttractorVals.prototype);
        obj.__wbg_ptr = ptr;
        AttractorValsFinalization.register(obj, obj.__wbg_ptr, obj);
        return obj;
    }

    __destroy_into_raw() {
        const ptr = this.__wbg_ptr;
        this.__wbg_ptr = 0;
        AttractorValsFinalization.unregister(this);
        return ptr;
    }

    free() {
        const ptr = this.__destroy_into_raw();
        wasm.__wbg_attractorvals_free(ptr, 0);
    }
    /**
     * @returns {number}
     */
    get x() {
        const ret = wasm.__wbg_get_attractorvals_x(this.__wbg_ptr);
        return ret;
    }
    /**
     * @param {number} arg0
     */
    set x(arg0) {
        wasm.__wbg_set_attractorvals_x(this.__wbg_ptr, arg0);
    }
    /**
     * @returns {number}
     */
    get y() {
        const ret = wasm.__wbg_get_attractorvals_y(this.__wbg_ptr);
        return ret;
    }
    /**
     * @param {number} arg0
     */
    set y(arg0) {
        wasm.__wbg_set_attractorvals_y(this.__wbg_ptr, arg0);
    }
    /**
     * @returns {number}
     */
    get z() {
        const ret = wasm.__wbg_get_attractorvals_z(this.__wbg_ptr);
        return ret;
    }
    /**
     * @param {number} arg0
     */
    set z(arg0) {
        wasm.__wbg_set_attractorvals_z(this.__wbg_ptr, arg0);
    }
    /**
     * @returns {number}
     */
    get vx() {
        const ret = wasm.__wbg_get_attractorvals_vx(this.__wbg_ptr);
        return ret;
    }
    /**
     * @param {number} arg0
     */
    set vx(arg0) {
        wasm.__wbg_set_attractorvals_vx(this.__wbg_ptr, arg0);
    }
    /**
     * @returns {number}
     */
    get vy() {
        const ret = wasm.__wbg_get_attractorvals_vy(this.__wbg_ptr);
        return ret;
    }
    /**
     * @param {number} arg0
     */
    set vy(arg0) {
        wasm.__wbg_set_attractorvals_vy(this.__wbg_ptr, arg0);
    }
    /**
     * @returns {number}
     */
    get vz() {
        const ret = wasm.__wbg_get_attractorvals_vz(this.__wbg_ptr);
        return ret;
    }
    /**
     * @param {number} arg0
     */
    set vz(arg0) {
        wasm.__wbg_set_attractorvals_vz(this.__wbg_ptr, arg0);
    }
    /**
     * @returns {string}
     */
    render() {
        let deferred1_0;
        let deferred1_1;
        try {
            const ret = wasm.attractorvals_render(this.__wbg_ptr);
            deferred1_0 = ret[0];
            deferred1_1 = ret[1];
            return getStringFromWasm0(ret[0], ret[1]);
        } finally {
            wasm.__wbindgen_free(deferred1_0, deferred1_1, 1);
        }
    }
}

const CanvasValsFinalization = (typeof FinalizationRegistry === 'undefined')
    ? { register: () => {}, unregister: () => {} }
    : new FinalizationRegistry(ptr => wasm.__wbg_canvasvals_free(ptr >>> 0, 1));

export class CanvasVals {

    static __wrap(ptr) {
        ptr = ptr >>> 0;
        const obj = Object.create(CanvasVals.prototype);
        obj.__wbg_ptr = ptr;
        CanvasValsFinalization.register(obj, obj.__wbg_ptr, obj);
        return obj;
    }

    __destroy_into_raw() {
        const ptr = this.__wbg_ptr;
        this.__wbg_ptr = 0;
        CanvasValsFinalization.unregister(this);
        return ptr;
    }

    free() {
        const ptr = this.__destroy_into_raw();
        wasm.__wbg_canvasvals_free(ptr, 0);
    }
    /**
     * @returns {number}
     */
    get x1() {
        const ret = wasm.__wbg_get_attractorvals_x(this.__wbg_ptr);
        return ret >>> 0;
    }
    /**
     * @param {number} arg0
     */
    set x1(arg0) {
        wasm.__wbg_set_attractorvals_x(this.__wbg_ptr, arg0);
    }
    /**
     * @returns {number}
     */
    get y1() {
        const ret = wasm.__wbg_get_attractorvals_y(this.__wbg_ptr);
        return ret >>> 0;
    }
    /**
     * @param {number} arg0
     */
    set y1(arg0) {
        wasm.__wbg_set_attractorvals_y(this.__wbg_ptr, arg0);
    }
    /**
     * @returns {number}
     */
    get x2() {
        const ret = wasm.__wbg_get_attractorvals_z(this.__wbg_ptr);
        return ret >>> 0;
    }
    /**
     * @param {number} arg0
     */
    set x2(arg0) {
        wasm.__wbg_set_attractorvals_z(this.__wbg_ptr, arg0);
    }
    /**
     * @returns {number}
     */
    get y2() {
        const ret = wasm.__wbg_get_attractorvals_vx(this.__wbg_ptr);
        return ret >>> 0;
    }
    /**
     * @param {number} arg0
     */
    set y2(arg0) {
        wasm.__wbg_set_attractorvals_vx(this.__wbg_ptr, arg0);
    }
    /**
     * @returns {number}
     */
    get t1() {
        const ret = wasm.__wbg_get_attractorvals_vy(this.__wbg_ptr);
        return ret >>> 0;
    }
    /**
     * @param {number} arg0
     */
    set t1(arg0) {
        wasm.__wbg_set_attractorvals_vy(this.__wbg_ptr, arg0);
    }
    /**
     * @returns {number}
     */
    get t2() {
        const ret = wasm.__wbg_get_attractorvals_vz(this.__wbg_ptr);
        return ret >>> 0;
    }
    /**
     * @param {number} arg0
     */
    set t2(arg0) {
        wasm.__wbg_set_attractorvals_vz(this.__wbg_ptr, arg0);
    }
    /**
     * @returns {number}
     */
    get w1() {
        const ret = wasm.__wbg_get_canvasvals_w1(this.__wbg_ptr);
        return ret >>> 0;
    }
    /**
     * @param {number} arg0
     */
    set w1(arg0) {
        wasm.__wbg_set_canvasvals_w1(this.__wbg_ptr, arg0);
    }
    /**
     * @returns {number}
     */
    get w2() {
        const ret = wasm.__wbg_get_canvasvals_w2(this.__wbg_ptr);
        return ret >>> 0;
    }
    /**
     * @param {number} arg0
     */
    set w2(arg0) {
        wasm.__wbg_set_canvasvals_w2(this.__wbg_ptr, arg0);
    }
}

const PendFinalization = (typeof FinalizationRegistry === 'undefined')
    ? { register: () => {}, unregister: () => {} }
    : new FinalizationRegistry(ptr => wasm.__wbg_pend_free(ptr >>> 0, 1));

export class Pend {

    static __wrap(ptr) {
        ptr = ptr >>> 0;
        const obj = Object.create(Pend.prototype);
        obj.__wbg_ptr = ptr;
        PendFinalization.register(obj, obj.__wbg_ptr, obj);
        return obj;
    }

    __destroy_into_raw() {
        const ptr = this.__wbg_ptr;
        this.__wbg_ptr = 0;
        PendFinalization.unregister(this);
        return ptr;
    }

    free() {
        const ptr = this.__destroy_into_raw();
        wasm.__wbg_pend_free(ptr, 0);
    }
    /**
     * @returns {number}
     */
    get t() {
        const ret = wasm.__wbg_get_anglevals_t1(this.__wbg_ptr);
        return ret;
    }
    /**
     * @param {number} arg0
     */
    set t(arg0) {
        wasm.__wbg_set_anglevals_t1(this.__wbg_ptr, arg0);
    }
    /**
     * @returns {number}
     */
    get w() {
        const ret = wasm.__wbg_get_anglevals_w1(this.__wbg_ptr);
        return ret;
    }
    /**
     * @param {number} arg0
     */
    set w(arg0) {
        wasm.__wbg_set_anglevals_w1(this.__wbg_ptr, arg0);
    }
    /**
     * @returns {number}
     */
    get l() {
        const ret = wasm.__wbg_get_anglevals_t2(this.__wbg_ptr);
        return ret;
    }
    /**
     * @param {number} arg0
     */
    set l(arg0) {
        wasm.__wbg_set_anglevals_t2(this.__wbg_ptr, arg0);
    }
    /**
     * @returns {number}
     */
    get m() {
        const ret = wasm.__wbg_get_anglevals_w2(this.__wbg_ptr);
        return ret;
    }
    /**
     * @param {number} arg0
     */
    set m(arg0) {
        wasm.__wbg_set_anglevals_w2(this.__wbg_ptr, arg0);
    }
    /**
     * @param {number} t
     * @param {number} w
     * @param {number} l
     * @param {number} m
     * @returns {Pend}
     */
    static create(t, w, l, m) {
        const ret = wasm.pend_create(t, w, l, m);
        return Pend.__wrap(ret);
    }
    /**
     * @returns {string}
     */
    render() {
        let deferred1_0;
        let deferred1_1;
        try {
            const ret = wasm.pend_render(this.__wbg_ptr);
            deferred1_0 = ret[0];
            deferred1_1 = ret[1];
            return getStringFromWasm0(ret[0], ret[1]);
        } finally {
            wasm.__wbindgen_free(deferred1_0, deferred1_1, 1);
        }
    }
}

const PointFinalization = (typeof FinalizationRegistry === 'undefined')
    ? { register: () => {}, unregister: () => {} }
    : new FinalizationRegistry(ptr => wasm.__wbg_point_free(ptr >>> 0, 1));

export class Point {

    static __wrap(ptr) {
        ptr = ptr >>> 0;
        const obj = Object.create(Point.prototype);
        obj.__wbg_ptr = ptr;
        PointFinalization.register(obj, obj.__wbg_ptr, obj);
        return obj;
    }

    __destroy_into_raw() {
        const ptr = this.__wbg_ptr;
        this.__wbg_ptr = 0;
        PointFinalization.unregister(this);
        return ptr;
    }

    free() {
        const ptr = this.__destroy_into_raw();
        wasm.__wbg_point_free(ptr, 0);
    }
    /**
     * @returns {number}
     */
    get x() {
        const ret = wasm.__wbg_get_anglevals_t1(this.__wbg_ptr);
        return ret;
    }
    /**
     * @param {number} arg0
     */
    set x(arg0) {
        wasm.__wbg_set_anglevals_t1(this.__wbg_ptr, arg0);
    }
    /**
     * @returns {number}
     */
    get y() {
        const ret = wasm.__wbg_get_anglevals_w1(this.__wbg_ptr);
        return ret;
    }
    /**
     * @param {number} arg0
     */
    set y(arg0) {
        wasm.__wbg_set_anglevals_w1(this.__wbg_ptr, arg0);
    }
    /**
     * @returns {number}
     */
    get z() {
        const ret = wasm.__wbg_get_anglevals_t2(this.__wbg_ptr);
        return ret;
    }
    /**
     * @param {number} arg0
     */
    set z(arg0) {
        wasm.__wbg_set_anglevals_t2(this.__wbg_ptr, arg0);
    }
}

async function __wbg_load(module, imports) {
    if (typeof Response === 'function' && module instanceof Response) {
        if (typeof WebAssembly.instantiateStreaming === 'function') {
            try {
                return await WebAssembly.instantiateStreaming(module, imports);

            } catch (e) {
                if (module.headers.get('Content-Type') != 'application/wasm') {
                    console.warn("`WebAssembly.instantiateStreaming` failed because your server does not serve Wasm with `application/wasm` MIME type. Falling back to `WebAssembly.instantiate` which is slower. Original error:\n", e);

                } else {
                    throw e;
                }
            }
        }

        const bytes = await module.arrayBuffer();
        return await WebAssembly.instantiate(bytes, imports);

    } else {
        const instance = await WebAssembly.instantiate(module, imports);

        if (instance instanceof WebAssembly.Instance) {
            return { instance, module };

        } else {
            return instance;
        }
    }
}

function __wbg_get_imports() {
    const imports = {};
    imports.wbg = {};
    imports.wbg.__wbindgen_init_externref_table = function() {
        const table = wasm.__wbindgen_export_0;
        const offset = table.grow(4);
        table.set(0, undefined);
        table.set(offset + 0, undefined);
        table.set(offset + 1, null);
        table.set(offset + 2, true);
        table.set(offset + 3, false);
        ;
    };
    imports.wbg.__wbindgen_throw = function(arg0, arg1) {
        throw new Error(getStringFromWasm0(arg0, arg1));
    };

    return imports;
}

function __wbg_init_memory(imports, memory) {

}

function __wbg_finalize_init(instance, module) {
    wasm = instance.exports;
    __wbg_init.__wbindgen_wasm_module = module;
    cachedUint8ArrayMemory0 = null;


    wasm.__wbindgen_start();
    return wasm;
}

function initSync(module) {
    if (wasm !== undefined) return wasm;


    if (typeof module !== 'undefined') {
        if (Object.getPrototypeOf(module) === Object.prototype) {
            ({module} = module)
        } else {
            console.warn('using deprecated parameters for `initSync()`; pass a single object instead')
        }
    }

    const imports = __wbg_get_imports();

    __wbg_init_memory(imports);

    if (!(module instanceof WebAssembly.Module)) {
        module = new WebAssembly.Module(module);
    }

    const instance = new WebAssembly.Instance(module, imports);

    return __wbg_finalize_init(instance, module);
}

async function __wbg_init(module_or_path) {
    if (wasm !== undefined) return wasm;


    if (typeof module_or_path !== 'undefined') {
        if (Object.getPrototypeOf(module_or_path) === Object.prototype) {
            ({module_or_path} = module_or_path)
        } else {
            console.warn('using deprecated parameters for the initialization function; pass a single object instead')
        }
    }

    if (typeof module_or_path === 'undefined') {
        module_or_path = new URL('least_action_bg.wasm', import.meta.url);
    }
    const imports = __wbg_get_imports();

    if (typeof module_or_path === 'string' || (typeof Request === 'function' && module_or_path instanceof Request) || (typeof URL === 'function' && module_or_path instanceof URL)) {
        module_or_path = fetch(module_or_path);
    }

    __wbg_init_memory(imports);

    const { instance, module } = await __wbg_load(await module_or_path, imports);

    return __wbg_finalize_init(instance, module);
}

export { initSync };
export default __wbg_init;
