/* tslint:disable */
/* eslint-disable */
export function calc_E(p1: Pend, p2: Pend): number;
export function calc_w_max(p1: Pend, p2: Pend, width: number): number;
export function rk4(p1: Pend, p2: Pend, dt: number): AngleVals;
export function normalize_to_canvas(p1: Pend, p2: Pend, w: number, wmax: number, wscale: number): CanvasVals;
export function rotate_and_center(a: AttractorVals, width: number, frame_count: number): AttractorVals;
export class AngleVals {
  private constructor();
  free(): void;
  t1: number;
  w1: number;
  t2: number;
  w2: number;
}
export class Attractor {
  private constructor();
  free(): void;
  static new(x: number, y: number, z: number, sigma: number, rho: number, beta: number): Attractor;
  update(): void;
  center(width: number): Point;
  scale_to_canvas(width: number, center: Point): AttractorVals;
  render(): string;
  x: number;
  y: number;
  z: number;
  vx: number;
  vy: number;
  vz: number;
  sigma: number;
  rho: number;
  beta: number;
}
export class AttractorVals {
  private constructor();
  free(): void;
  render(): string;
  x: number;
  y: number;
  z: number;
  vx: number;
  vy: number;
  vz: number;
}
export class CanvasVals {
  private constructor();
  free(): void;
  x1: number;
  y1: number;
  x2: number;
  y2: number;
  t1: number;
  t2: number;
  w1: number;
  w2: number;
}
export class Pend {
  private constructor();
  free(): void;
  static create(t: number, w: number, l: number, m: number): Pend;
  render(): string;
  t: number;
  w: number;
  l: number;
  m: number;
}
export class Point {
  private constructor();
  free(): void;
  x: number;
  y: number;
  z: number;
}

export type InitInput = RequestInfo | URL | Response | BufferSource | WebAssembly.Module;

export interface InitOutput {
  readonly memory: WebAssembly.Memory;
  readonly __wbg_pend_free: (a: number, b: number) => void;
  readonly pend_create: (a: number, b: number, c: number, d: number) => number;
  readonly pend_render: (a: number) => [number, number];
  readonly calc_E: (a: number, b: number) => number;
  readonly calc_w_max: (a: number, b: number, c: number) => number;
  readonly __wbg_anglevals_free: (a: number, b: number) => void;
  readonly __wbg_get_anglevals_t1: (a: number) => number;
  readonly __wbg_set_anglevals_t1: (a: number, b: number) => void;
  readonly __wbg_get_anglevals_w1: (a: number) => number;
  readonly __wbg_set_anglevals_w1: (a: number, b: number) => void;
  readonly __wbg_get_anglevals_t2: (a: number) => number;
  readonly __wbg_set_anglevals_t2: (a: number, b: number) => void;
  readonly __wbg_get_anglevals_w2: (a: number) => number;
  readonly __wbg_set_anglevals_w2: (a: number, b: number) => void;
  readonly rk4: (a: number, b: number, c: number) => number;
  readonly __wbg_canvasvals_free: (a: number, b: number) => void;
  readonly __wbg_get_canvasvals_w1: (a: number) => number;
  readonly __wbg_set_canvasvals_w1: (a: number, b: number) => void;
  readonly __wbg_get_canvasvals_w2: (a: number) => number;
  readonly __wbg_set_canvasvals_w2: (a: number, b: number) => void;
  readonly normalize_to_canvas: (a: number, b: number, c: number, d: number, e: number) => number;
  readonly __wbg_attractorvals_free: (a: number, b: number) => void;
  readonly __wbg_get_attractorvals_x: (a: number) => number;
  readonly __wbg_set_attractorvals_x: (a: number, b: number) => void;
  readonly __wbg_get_attractorvals_y: (a: number) => number;
  readonly __wbg_set_attractorvals_y: (a: number, b: number) => void;
  readonly __wbg_get_attractorvals_z: (a: number) => number;
  readonly __wbg_set_attractorvals_z: (a: number, b: number) => void;
  readonly __wbg_get_attractorvals_vx: (a: number) => number;
  readonly __wbg_set_attractorvals_vx: (a: number, b: number) => void;
  readonly __wbg_get_attractorvals_vy: (a: number) => number;
  readonly __wbg_set_attractorvals_vy: (a: number, b: number) => void;
  readonly __wbg_get_attractorvals_vz: (a: number) => number;
  readonly __wbg_set_attractorvals_vz: (a: number, b: number) => void;
  readonly __wbg_point_free: (a: number, b: number) => void;
  readonly attractorvals_render: (a: number) => [number, number];
  readonly __wbg_attractor_free: (a: number, b: number) => void;
  readonly __wbg_get_attractor_vy: (a: number) => number;
  readonly __wbg_set_attractor_vy: (a: number, b: number) => void;
  readonly __wbg_get_attractor_vz: (a: number) => number;
  readonly __wbg_set_attractor_vz: (a: number, b: number) => void;
  readonly __wbg_get_attractor_sigma: (a: number) => number;
  readonly __wbg_set_attractor_sigma: (a: number, b: number) => void;
  readonly __wbg_get_attractor_rho: (a: number) => number;
  readonly __wbg_set_attractor_rho: (a: number, b: number) => void;
  readonly __wbg_get_attractor_beta: (a: number) => number;
  readonly __wbg_set_attractor_beta: (a: number, b: number) => void;
  readonly attractor_new: (a: number, b: number, c: number, d: number, e: number, f: number) => number;
  readonly attractor_update: (a: number) => void;
  readonly attractor_center: (a: number, b: number) => number;
  readonly attractor_scale_to_canvas: (a: number, b: number, c: number) => number;
  readonly attractor_render: (a: number) => [number, number];
  readonly rotate_and_center: (a: number, b: number, c: number) => number;
  readonly __wbg_get_pend_t: (a: number) => number;
  readonly __wbg_get_pend_w: (a: number) => number;
  readonly __wbg_get_pend_l: (a: number) => number;
  readonly __wbg_get_pend_m: (a: number) => number;
  readonly __wbg_get_canvasvals_x1: (a: number) => number;
  readonly __wbg_get_canvasvals_y1: (a: number) => number;
  readonly __wbg_get_canvasvals_x2: (a: number) => number;
  readonly __wbg_get_canvasvals_y2: (a: number) => number;
  readonly __wbg_get_canvasvals_t1: (a: number) => number;
  readonly __wbg_get_canvasvals_t2: (a: number) => number;
  readonly __wbg_get_point_x: (a: number) => number;
  readonly __wbg_get_point_y: (a: number) => number;
  readonly __wbg_get_point_z: (a: number) => number;
  readonly __wbg_get_attractor_x: (a: number) => number;
  readonly __wbg_get_attractor_y: (a: number) => number;
  readonly __wbg_get_attractor_z: (a: number) => number;
  readonly __wbg_get_attractor_vx: (a: number) => number;
  readonly __wbg_set_pend_t: (a: number, b: number) => void;
  readonly __wbg_set_pend_w: (a: number, b: number) => void;
  readonly __wbg_set_pend_l: (a: number, b: number) => void;
  readonly __wbg_set_pend_m: (a: number, b: number) => void;
  readonly __wbg_set_canvasvals_x1: (a: number, b: number) => void;
  readonly __wbg_set_canvasvals_y1: (a: number, b: number) => void;
  readonly __wbg_set_canvasvals_x2: (a: number, b: number) => void;
  readonly __wbg_set_canvasvals_y2: (a: number, b: number) => void;
  readonly __wbg_set_canvasvals_t1: (a: number, b: number) => void;
  readonly __wbg_set_canvasvals_t2: (a: number, b: number) => void;
  readonly __wbg_set_point_x: (a: number, b: number) => void;
  readonly __wbg_set_point_y: (a: number, b: number) => void;
  readonly __wbg_set_point_z: (a: number, b: number) => void;
  readonly __wbg_set_attractor_x: (a: number, b: number) => void;
  readonly __wbg_set_attractor_y: (a: number, b: number) => void;
  readonly __wbg_set_attractor_z: (a: number, b: number) => void;
  readonly __wbg_set_attractor_vx: (a: number, b: number) => void;
  readonly __wbindgen_export_0: WebAssembly.Table;
  readonly __wbindgen_free: (a: number, b: number, c: number) => void;
  readonly __wbindgen_start: () => void;
}

export type SyncInitInput = BufferSource | WebAssembly.Module;
/**
* Instantiates the given `module`, which can either be bytes or
* a precompiled `WebAssembly.Module`.
*
* @param {{ module: SyncInitInput }} module - Passing `SyncInitInput` directly is deprecated.
*
* @returns {InitOutput}
*/
export function initSync(module: { module: SyncInitInput } | SyncInitInput): InitOutput;

/**
* If `module_or_path` is {RequestInfo} or {URL}, makes a request and
* for everything else, calls `WebAssembly.instantiate` directly.
*
* @param {{ module_or_path: InitInput | Promise<InitInput> }} module_or_path - Passing `InitInput` directly is deprecated.
*
* @returns {Promise<InitOutput>}
*/
export default function __wbg_init (module_or_path?: { module_or_path: InitInput | Promise<InitInput> } | InitInput | Promise<InitInput>): Promise<InitOutput>;
