import init, {Pend, rk4, calc_w_max, normalize_to_canvas, 
    Attractor, AttractorVals, rotate_and_center} from './pkg/least_action.js';
await init();

const pcanvas = document.getElementById("pend_sim");
const pctx = pcanvas.getContext("2d");
const acanvas = document.getElementById("attractor_sim");
const actx = acanvas.getContext("2d");
document.getElementById('start_pend').onmousedown = start_pend;
document.getElementById('stop_pend').onmousedown = stop_pend;
document.getElementById('start_lorenz').onmousedown = start_lorenz;
document.getElementById('stop_lorenz').onmousedown = stop_lorenz;

const decimal =  /[+-]?(?:\d*\.)?\d+/; 
const width = Math.min(Math.min(window.innerWidth, window.innerHeight)*15/16, 800);
const height = width;
const radius = width/100;
// Need to scale the colors on the Lorenz attractor
// so the "depth" appears the same on different sizes.
const colorscale = 800/width;
document.getElementById("pend_sim").width = width;
document.getElementById("pend_sim").height = height;
document.getElementById("attractor_sim").width = width;
document.getElementById("attractor_sim").height = height;

// Double pendulum
let praf;
let dt = 1/60;
const N = 500;
let p1;
let p2;
let t1s;
let w1s;
let t2s;
let w2s;
let wscale;
let wmax;
let theta1 = 3.13;
let omega1 = 1.0;
let theta2 = 3.13;
let omega2 = -3.0;
let lbar = 1;
let mbar = 1;
let plot_t = true;
let plot_w = true;
document.getElementById("theta1").value = theta1;
document.getElementById("omega1").value = omega1;
document.getElementById("theta2").value = theta2;
document.getElementById("omega2").value = omega2;
document.getElementById("lbar").value = lbar;
document.getElementById("mbar").value = mbar;
document.getElementById("plot_t").checked = plot_t;
document.getElementById("plot_w").checked = plot_w;
document.getElementById("plot_t").addEventListener('click', update_cb_vals);
document.getElementById("plot_w").addEventListener('click', update_cb_vals);
let pend_running = false;


// Lorenz attractor
const Nl = 1000;
let araf;
let a;
let avals;
let rot_vals;
let center;
let frame_count = 0;
let x0 = -13.8;
let y0 = -10.6;
let z0 = 36.8;
let sigma = 10;
let rho = 25;
let beta = 7/3;
let plot_coords = true;
let plot_vels = true
document.getElementById("x0").value = x0;
document.getElementById("y0").value = y0;
document.getElementById("z0").value = z0;
document.getElementById("sigma").value = sigma;
document.getElementById("rho").value = rho;
document.getElementById("beta").value = beta;
document.getElementById("plot_coords").checked = plot_coords;
document.getElementById("plot_vels").checked = plot_vels;
document.getElementById("plot_coords").addEventListener('click', update_cb_vals);
document.getElementById("plot_vels").addEventListener('click', update_cb_vals);
let lorenz_running = false;

function update_cb_vals() {
    plot_t = document.getElementById("plot_t").checked;
    plot_w = document.getElementById("plot_w").checked;
    plot_coords = document.getElementById("plot_coords").checked;
    plot_vels = document.getElementById("plot_vels").checked;
}

function validate_input(name) {
    const value = document.getElementById(name).value;
    if (!value.match(decimal)) {
        document.getElementById(name).value = "Numbers only :)";
        return false;
    }
    return true;
}

function retrieve_inputs_pend() {
    const input_names = ["theta1", "omega1", "theta2", "omega2", "lbar", "mbar"];
    theta1 = document.getElementById("theta1").value;
    omega1 = document.getElementById("omega1").value;
    theta2 = document.getElementById("theta2").value;
    omega2 = document.getElementById("omega2").value;
    lbar = document.getElementById("lbar").value;
    mbar = document.getElementById("mbar").value;
    plot_t = document.getElementById("plot_t").checked;
    plot_w = document.getElementById("plot_w").checked;
    for (let i = 0; i < input_names.length; i++) {
        if (!validate_input(input_names[i])) {
            return false
        }
    }
    return true;

}
function retrieve_inputs_lorenz() {
    const input_names = ["x0", "y0", "z0", "sigma", "rho", "beta"];
    x0 = document.getElementById("x0").value;
    y0 = document.getElementById("y0").value;
    z0 = document.getElementById("z0").value;
    sigma = document.getElementById("sigma").value;
    rho = document.getElementById("rho").value;
    beta = document.getElementById("beta").value;
    plot_coords = document.getElementById("plot_coords").checked;
    plot_vels = document.getElementById("plot_vels").checked;
    for (let i = 0; i < input_names.length; i++) {
        if (!validate_input(input_names[i])) {
            return false
        }
    }
    return true;
}

function set_inputs() {
    // Double pendulum
    document.getElementById("theta1").value = p1.t % (2 * Math.PI);
    document.getElementById("omega1").value = p1.w;
    document.getElementById("theta2").value = p2.t % (2 * Math.PI);
    document.getElementById("omega2").value = p2.w;
    document.getElementById("lbar").value = lbar;
    document.getElementById("mbar").value = mbar;
    document.getElementById("plot_t").checked = plot_t;
    document.getElementById("plot_w").checked = plot_w;
    // Lorenz attractor
    document.getElementById("x0").value = a.x;
    document.getElementById("y0").value = a.y;
    document.getElementById("z0").value = a.z;
    document.getElementById("sigma").value = a.sigma;
    document.getElementById("rho").value = a.rho;
    document.getElementById("beta").value = a.beta;
    document.getElementById("plot_coords").checked = plot_coords;
    document.getElementById("plot_vels").checked = plot_vels;
}

function start_pend() {
    if (!retrieve_inputs_pend()) {
        alert("Invalid input.");
        return 0
    }
    p1 = Pend.create(theta1, omega1, 1, 1);
    p2 = Pend.create(theta2, omega2, lbar, mbar);
    wmax = calc_w_max(p1, p2, width);
    // Dunno what's wrong with the calculation. Just fudge the numbers
    // so it looks nice.
    // TODO: fix this maybe
    wscale = 10*width/(2*wmax);
    const vals = normalize_to_canvas(p1, p2, width, wmax, wscale);
    t1s = Array(N).fill(vals.t1);
    w1s = Array(N).fill(vals.w1);
    t2s = Array(N).fill(vals.t2);
    w2s = Array(N).fill(vals.w2);
    if (!pend_running) {
        requestAnimationFrame(draw_pend);
        pend_running = true;
    }
    stop_lorenz();
}

function stop_pend() {
    if (pend_running) {
        cancelAnimationFrame(praf);
        pend_running = false;
        set_inputs();
    }
}

function update_pend() {
    const new_vals = rk4(p1, p2, dt);
    p1.t = new_vals.t1;
    p1.w = new_vals.w1;
    p2.t = new_vals.t2;
    p2.w = new_vals.w2;
}

function draw_pend() {
    const vals = normalize_to_canvas(p1, p2, width, wmax, wscale);
    pctx.clearRect(0, 0, 800, 800);

    // Draw line to first pend
    pctx.beginPath();
    pctx.strokeStyle = `rgb(50, 50, 50)`;
    pctx.moveTo(width/2, width/2);
    pctx.lineTo(vals.x1, vals.y1);
    pctx.closePath();
    pctx.stroke();

    // Draw line to second pend
    pctx.beginPath();
    pctx.moveTo(vals.x1, vals.y1);
    pctx.lineTo(vals.x2, vals.y2);
    pctx.closePath();
    pctx.stroke();

    // Update arrays of all angular values
    t1s.pop();
    w1s.pop();
    t2s.pop();
    w2s.pop();
    t1s.unshift(vals.t1);
    w1s.unshift(vals.w1);
    t2s.unshift(vals.t2);
    w2s.unshift(vals.w2);

    // Plot angles all fancy
    if (plot_t) {
        pctx.beginPath();
        pctx.shadowBlur = 1;
        pctx.shadowColor = `rgb(20, 220, 50)`;
        pctx.strokeStyle = `rgb(20, 220, 50)`;
        pctx.moveTo(t1s[0], t2s[0]);
        for (let i = 1; i < N; i++) {
            if (Math.abs(t1s[i] - t1s[i-1]) < 100 && Math.abs(t2s[i] - t2s[i-1]) < 100) {
                pctx.lineTo(t1s[i], t2s[i]);
            } else {
                pctx.moveTo(t1s[i], t2s[i]);
            }
        }
        pctx.stroke();
    }
    
    // Plot angular velocities
    if (plot_w) {
        pctx.beginPath();
        pctx.shadowBlur = 1;
        pctx.shadowColor = `rgb(20, 50, 220)`;
        pctx.strokeStyle = `rgb(20, 50, 220)`;
        pctx.moveTo(w1s[0], w2s[0]);
        for (let i = 1; i < N; i++) {
            pctx.lineTo(w1s[i], w2s[i]);
        }
        pctx.stroke();
    }

    // Draw both pendulum circles
    pctx.shadowBlur = 0;
    pctx.fillStyle = `rgb(50, 100, 140)`;
    pctx.beginPath();
    pctx.arc(vals.x1, vals.y1, radius, 0, Math.PI*2, true);
    pctx.closePath();
    pctx.fill();
    pctx.beginPath();
    pctx.arc(vals.x2, vals.y2, radius, 0, Math.PI*2, true);
    pctx.closePath();
    pctx.fill();

    // Run rk4
    update_pend();
    praf = requestAnimationFrame(draw_pend);
}

function start_lorenz() {
    if (!retrieve_inputs_lorenz()) {
        alert("Invalid input.");
        return 0
    }
    a = Attractor.new(x0, y0, z0, sigma, rho, beta);
    center = a.center(width);
    const aval = a.scale_to_canvas(width, center);
    avals = Array(Nl).fill(aval);
    rot_vals = Array(Nl).fill(aval);
    if (!lorenz_running) {
        requestAnimationFrame(draw_lorenz);
        lorenz_running = true;
    }
    stop_pend();
}

function stop_lorenz() {
    if (lorenz_running) {
        cancelAnimationFrame(araf);
        lorenz_running = false;
        set_inputs();
    }
}

function draw_lorenz() {
    frame_count++;
    actx.clearRect(0, 0, 800, 800);
    const aval = a.scale_to_canvas(width, center);
    avals.pop();
    avals.unshift(aval);

    // rotate all the vals
    for (let i = 0; i < Nl; i++) {
        let new_vals = rotate_and_center(avals[i], width, frame_count);
        rot_vals.pop()
        rot_vals.unshift(new_vals)
    }

    // Draw coordinates
    if (plot_coords) {
        actx.beginPath();
        actx.moveTo(rot_vals[0].x, rot_vals[0].y);
        for (let i = 1; i < Nl; i++) {
            actx.strokeStyle = `rgb(${rot_vals[i].z/2*colorscale}, 40, 200)`;
            actx.lineTo(rot_vals[i].x, rot_vals[i].y);
            actx.stroke();
            actx.beginPath();
            actx.moveTo(rot_vals[i].x, rot_vals[i].y);
        }
        actx.stroke();
    }
    
    // Draw velocities
    if (plot_vels) {
        actx.beginPath();
        actx.moveTo(rot_vals[0].vx, rot_vals[0].vy);
        for (let i = 1; i < Nl; i++) {
            actx.strokeStyle = `rgb(220, ${rot_vals[i].vz/4*colorscale}, 40)`;
            actx.lineTo(rot_vals[i].vx, rot_vals[i].vy);
            actx.stroke();
            actx.beginPath();
            actx.moveTo(rot_vals[i].vx, rot_vals[i].vy);
        }
        actx.stroke();
    }

    a.update();
    araf = requestAnimationFrame(draw_lorenz);
}
