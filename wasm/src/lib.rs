use core::fmt;
use std::f32::consts::PI;
use wasm_bindgen::prelude::*;
const g: f32 = 9.81;

#[wasm_bindgen]
#[derive(Default, Debug)]
#[derive(Clone)]
pub struct Pend {
    pub t: f32,
    pub w: f32,
    pub l: f32,
    pub m: f32,
}

#[wasm_bindgen]
impl Pend {
    pub fn create(t: f32, w: f32, l: f32, m: f32) -> Self {
        Self {
            t, w, l, m
        }
    }

    pub fn render(&self) -> String {
        self.to_string()
    }
}

#[wasm_bindgen]
pub fn calc_E(p1: &Pend, p2: &Pend) -> f32 {
    let T = (p1.m + p2.m)/2.0*(p1.l.powi(2)*p1.w.powi(2)) + p2.m*p1.l*p2.l*p1.w*p2.w*(p1.t-p2.t).cos() + p2.m/2.0*p2.l.powi(2)*p2.w.powi(2);
    let U = (p1.m + p2.m)*g*p1.l*(1.0-p1.t.cos()) + p2.m*g*p2.l*(1.0-p2.t.cos());
    T + U
}

#[wasm_bindgen]
pub fn calc_w_max(p1: &Pend, p2: &Pend, width: f32) -> f32 {
    let T = (p1.m + p2.m)/2.0*(p1.l.powi(2)*p1.w.powi(2)) + p2.m*p1.l*p2.l*p1.w*p2.w*(p1.t-p2.t).cos() + p2.m/2.0*p2.l.powi(2)*p2.w.powi(2);
    let U = (p1.m + p2.m)*g*p1.l*(1.0-p1.t.cos()) + p2.m*g*p2.l*(1.0-p2.t.cos());
    let E = (T + U);
    let Umax = ((p1.m + p2.m)*g*p1.l + p2.m*g*p2.l);
    // Max of w1max and w2max
    (2.0*(E + Umax)/(p1.m*p1.l.powi(2))).max(2.0*(E + Umax)/(p2.m*p2.l.powi(2)))
}
impl fmt::Display for Pend {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Pendulum data: ϑ: {}, ω: {}, l: {}, m: {}", self.t, self.w, self.l, self.m)?;
        Ok(())
    }
}

fn a1(p1: &Pend, p2: &Pend) -> f32 {
    println!("{}", p1);
    let dt = p1.t - p2.t;
    return (-p2.m * dt.cos() * p1.l * p1.w.powi(2) * dt.sin() + p2.m * dt.cos() * g * p2.t.sin()
        - p2.m * p2.l * p2.w.powi(2) * dt.sin()
        - (p1.m + p2.m) * g * p1.t.sin())
        / (p1.l * (p1.m + p2.m - p2.m * dt.cos().powi(2)));
}

fn a2(p1: &Pend, p2: &Pend) -> f32 {
    let dt = p1.t - p2.t;
    return (p1.m + p2.m)
        * (p1.l * p1.w.powi(2) * dt.sin()
            + (p2.w.powi(2) * dt.sin() * dt.cos() * p2.m * p2.l) / (p1.m + p2.m)
            + dt.cos() * g * p1.t.sin()
            - g * p2.t.sin())
        / (p2.l * (p1.m + p2.m * dt.sin().powi(2)));
}

#[wasm_bindgen]
pub struct AngleVals {
    pub t1: f32,
    pub w1: f32,
    pub t2: f32,
    pub w2: f32,
}


#[wasm_bindgen]
pub fn rk4(p1: &Pend, p2: &Pend, dt: f32) -> AngleVals {
    let (mut pq1, mut pq2) = (p1.clone(), p2.clone());
    let k0x1 = a1(&p1, &p2);
    let k0x2 = a2(&p1, &p2);
    pq1.t = p1.t + dt / 2.0 * p1.w;
    pq1.w = p1.w + dt / 2.0 * k0x1;
    pq2.t = p2.t + dt / 2.0 * p2.w;
    pq2.w = p2.w + dt / 2.0 * k0x2;
    let k1x1 = a1(&pq1, &pq2);
    let k1x2 = a2(&pq1, &pq2);
    pq1.t = p1.t + dt / 2.0 * pq1.w;
    pq1.w = p1.w + dt / 2.0 * k1x1;
    pq2.t = p2.t + dt / 2.0 * pq2.w;
    pq2.w = p2.w + dt / 2.0 * k1x2;
    let k2x1 = a1(&pq1, &pq2);
    let k2x2 = a2(&pq1, &pq2);
    pq1.t = p1.t + dt * pq1.w;
    pq1.w = p1.w + dt * k2x1;
    pq2.t = p2.t + dt * pq2.w;
    pq2.w = p2.w + dt * k2x2;
    let k3x1 = a1(&pq1, &pq2);
    let k3x2 = a2(&pq1, &pq2);
    let t1 = p1.t + dt * (p1.w + dt / 6.0 * (k0x1 + k1x1 + k2x1));
    let t2 = p2.t + dt * (p2.w + dt / 6.0 * (k0x2 + k1x2 + k2x2));
    let w1 = p1.w + dt / 6.0 * (k0x1 + 2.0 * k1x1 + 2.0 * k2x1 + k3x1);
    let w2 = p2.w + dt / 6.0 * (k0x2 + 2.0 * k1x2 + 2.0 * k2x2 + k3x2);
    AngleVals {t1, w1, t2, w2}
}
/*
pub fn run_pend_sim(t1: f32, w1: f32, t2: f32, w2: f32, lbar: f32, mbar: f32, n: u32, w: u32) -> ReturnVals {
    // mbar = m2/m1
    // lbar = l2/l1
    let mut p1 = Pend {
        t: t1,
        w: w1,
        l: 1.0,
        m: 1.0,
    };
    let mut p2 = Pend {
        t: t2,
        w: w2,
        l: lbar,
        m: mbar,
    };

    let mut data: Vec<(f32, f32, f32, f32)> = Vec::new();
    for _ in 0..n {
        (p1.t, p1.w, p2.t, p2.w) = rk4(&p1, &p2, 0.01);
        data.push((p1.t, p1.w, p2.t, p2.w));
    }
    return normalize_to_canvas(data, lbar, w)
} */

#[wasm_bindgen]
pub struct CanvasVals {
    pub x1: u32,
    pub y1: u32,
    pub x2: u32,
    pub y2: u32,
    pub t1: u32,
    pub t2: u32,
    pub w1: u32,
    pub w2: u32,
}


#[wasm_bindgen]
pub fn normalize_to_canvas(p1: &Pend, p2: &Pend, w: f32, wmax: f32, wscale: f32) -> CanvasVals {
    // Note: w is in pixels
    let lbar = p2.l/p1.l;
    let l1: f32 = 0.9*w/(1.0 + lbar)/2.0;
    let l2: f32 = 0.9*w/2.0 - l1;
    let offset= w/2.0;
    let x1 = ((l1 * (p1.t).sin()) + offset) as u32;
    let y1 = ((l1 * (p1.t).cos()) + offset) as u32;
    let x2 = ((l1 * (p1.t).sin() + l2 * (p2.t).sin()) + offset) as u32;
    let y2 = ((l1 * (p1.t).cos() + l2 * (p2.t).cos()) + offset) as u32;
    // The modulo can return a negative number. Taking t % 2pi brings us to the 
    // range (-2pi, 2pi). Adding 2pi takes us to (0, 4pi). Taking % 2pi again
    // brings us to (0, 2pi) again. By then adding pi and taking % 2pi,
    // we shift things so that what was originally 0 and 2pi are both equal
    // to pi.
    let t1 = (((((p1.t % (2.0 * PI) + 2.0 * PI) % (2.0 * PI) + PI) % (2.0*PI)))/(2.0*PI)*w) as u32;
    let t2 = (((((p2.t % (2.0 * PI) + 2.0 * PI) % (2.0 * PI) + PI) % (2.0*PI)))/(2.0*PI)*w) as u32;
    let w1 = ((p1.w) * wscale + offset) as u32;
    let w2 = ((p2.w) * wscale + offset) as u32;

    CanvasVals { x1, y1, x2, y2, t1, t2, w1, w2 }
}

#[wasm_bindgen]
pub struct AttractorVals {
    pub x: i32,
    pub y: i32,
    pub z: i32,
    pub vx: i32,
    pub vy: i32,
    pub vz: i32,
}

#[wasm_bindgen]
pub struct Point {
    pub x: f32,
    pub y: f32,
    pub z: f32,
}

#[wasm_bindgen]
impl AttractorVals {
    pub fn render(&self) -> String {
        self.to_string()
    }
}

impl fmt::Display for AttractorVals {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Attractor data: x: {}, y: {}, z: {}, vx: {}, vy: {}, vx: {}", self.x, self.y, self.z, self.vx, self.vy, self.vz)?;
        Ok(())
    }
}

#[wasm_bindgen]
pub struct Attractor {
    pub x: f32,
    pub y: f32,
    pub z: f32,
    pub vx: f32,
    pub vy: f32,
    pub vz: f32,
    pub sigma: f32,
    pub rho: f32,
    pub beta: f32,
}



#[wasm_bindgen]
impl Attractor {
    pub fn new(x: f32, y: f32, z: f32, sigma: f32, rho: f32, beta: f32) -> Self {
        Self {
            x, 
            y, 
            z, 
            vx: sigma*(y-x),
            vy: x*(rho-z)-y,
            vz: x*y - beta*z,
            sigma,
            rho, 
            beta,
        }
    }

    pub fn update(&mut self) {
        let dt = 1.0/(self.vx.powi(2) + self.vy.powi(2) + self.vz.powi(2)).sqrt();
        self.vx = self.sigma * (self.y - self.x);
        self.x = self.x + self.vx * dt;
        self.vy = self.x*(self.rho - self.z) - self.y;
        self.y = self.y + self.vy * dt;
        self.vz = self.x * self.y - self.beta * self.z;
        self.z = self.z + self.vz * dt;
    }
    
    pub fn center(&self, width: f32) -> Point {
        let scale = width/80.0;
        let cx1 = (self.beta*(self.rho-1.0)).sqrt();
        let cy1 = cx1;
        let cz1 = self.rho - 1.0;
        let cx2 = -cx1;
        let cy2 = -cy1;
        let cz2 = cz1;
        // x and y are zero. Meh.
        Point {
            x: (cx1 + cx2)/2.0 * scale,
            y: (cy1 + cy2)/2.0 * scale,
            z: (cz1 + cz2)/2.0 * scale,
        }
    }

    pub fn scale_to_canvas(&mut self, width: f32, center: &Point) -> AttractorVals {
        let scale = width/80.0;
        let vscale = width/800.0;
        AttractorVals {
            x: ((self.x*scale)) as i32,
            y: ((self.y*scale)) as i32,
            z: ((self.z*scale) - center.z) as i32,
            vx: (self.vx*vscale) as i32,
            vy: (self.vy*vscale) as i32,
            vz: (self.vz*vscale) as i32,
        }
    }

    pub fn render(&self) -> String {
        self.to_string()
    }
}

impl fmt::Display for Attractor {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Attractor data: x: {}, y: {}, z: {}, vx: {}, vy: {}, vx: {}", self.x, self.y, self.z, self.vx, self.vy, self.vz)?;
        Ok(())
    }
}

#[wasm_bindgen]
pub fn rotate_and_center(a: &AttractorVals, width: f32, frame_count: u32) -> AttractorVals {
    let gamma = 0.01 * frame_count as f32;
    let beta = 2.0 * gamma;
    let offset = (width/2.0) as i32;
    //Ry(beta)Rx(gamma) = [cos(beta),  sin(beta)sin(gamma) , sin(beta)cos(gamma)
    //                     0,          cos(gamma),           -sin(gamma)
    //                     -sin(beta), cos(beta)sin(gamma),  cos(beta)cos(gamma)]
    AttractorVals {
        x: (beta.cos()*(a.x as f32) + beta.sin()*gamma.sin()*(a.y as f32) + beta.sin()*gamma.cos()*(a.z as f32)) as i32 + offset,
        y: (gamma.cos()*(a.y as f32) - gamma.sin()*(a.z as f32)) as i32 + offset,
        z: (-beta.sin()*(a.x as f32) + beta.cos()*gamma.sin()*(a.y as f32) + beta.cos()*gamma.cos()*(a.z as f32)) as i32 + offset,
        vx: (beta.cos()*(a.vx as f32) + beta.sin()*gamma.sin()*(a.vy as f32) + beta.sin()*gamma.cos()*(a.vz as f32)) as i32 + offset,
        vy: (gamma.cos()*(a.vy as f32) - gamma.sin()*(a.vz as f32)) as i32 + offset,
        vz: (-beta.sin()*(a.vx as f32) + beta.cos()*gamma.sin()*(a.vy as f32) + beta.cos()*gamma.cos()*(a.vz as f32)) as i32 + offset,
    }
}
